require_relative './classes/Usuario.rb'
require_relative './classes/Aluno.rb'
require_relative './classes/Professor.rb'
require_relative './classes/Turma.rb'
require_relative './classes/Materia.rb'

aluno1 = Aluno.new(email: "lucas@gmail.com", senha: "senha123", nome: "Lucas", nascimento: "09/08/2001", matricula:"M8301721", periodo: "3", curso:"Sistemas de Informação")

professor1 = Professor.new(email: "prof@gmail.com", senha: "senha123", nome: "Carlos", nascimento: "27/01/1969", matricula:"M8307223", salario:"4250.5")

turma1 = Turma.new(nome: "OB176", horario:"18h às 20h", dias_da_semana: ["Segunda", "Quarta"])

materia1 = Materia.new(nome: "Programação 1", ementa:"Ementa de prog 1")

# aluno1.inscrever(turma1)
# puts aluno1.turmas

# professor1.adicionar_materia(materia1)
# materia1.adicionar_professor(professor1)
# puts professor1.materias
# puts materia1.professores

# puts turma1.inscricao_aberta
# turma1.abrir_inscricao()
# puts turma1.inscricao_aberta
# turma1.fechar_inscricao()
# puts turma1.inscricao_aberta

# turma1.adicionar_aluno(aluno1)
# puts turma1.inscritos

# aluno1.logar("senha123")
# aluno1.deslogar()
# aluno1.logar("senha errada")

# professor1.logar("senha123")
# professor1.deslogar()
# professor1.logar("senha errada")

# puts aluno1.idade()
# puts professor1.idade()