class Turma

    attr_accessor :nome, :horario, :dias_da_semana, :inscricao_aberta, :inscritos

    def initialize(nome:, horario:, dias_da_semana:)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias_da_semana
        @inscritos = []
        @inscricao_aberta = false
    end

    def abrir_inscricao()
        @inscricao_aberta = true
        puts "Inscrição da turma #{@nome} aberta"
    end
    
    def fechar_inscricao()
        @inscricao_aberta = false
        puts "Inscrição da turma #{@nome} fechada"
    end
    
    def adicionar_aluno(aluno)
        @inscritos = @inscritos.push(aluno)
        puts("Aluno #{aluno.nome} cadatrado")
    end

end