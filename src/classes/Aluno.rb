require_relative './Usuario.rb'

class Aluno < Usuario
    
    attr_accessor :matricula, :periodo, :curso, :turmas

    def initialize(email:, senha:, nome:, nascimento:, matricula:, periodo:, curso:)
        super(email: email, senha:senha, nome:nome, nascimento:nascimento)
        @matricula = matricula
        @periodo = periodo
        @curso = curso
        @turmas = []
    end

    def inscrever(turma)
        @turmas = @turmas.push(turma)
        puts "Aluno #{@nome} inscrito na turma #{turma.nome}"
    end

end