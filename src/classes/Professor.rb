require_relative './Usuario.rb'

class Professor < Usuario

    attr_accessor :matricula, :materias, :salario

    def initialize(email:, senha:, nome:, nascimento:, matricula:, salario:)
        super(email: email, senha:senha, nome:nome, nascimento:nascimento)
        @matricula = matricula
        @salario = salario.to_f
        @materias = []
    end

    def adicionar_materia(materia)
        @materias = @materias.push(materia.nome)
        puts "Professor #{@nome} agora pode lecionar #{materia.nome}"
    end

end 