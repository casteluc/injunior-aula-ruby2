class Materia

    attr_accessor :nome, :ementa, :professores
    
    def initialize(ementa:, nome:)
        @ementa = ementa
        @nome = nome
        @professores = []
    end

    def adicionar_professor(professor)
        @professores = @professores.push(professor)
        puts "#{@nome} agora pode ser lecionada pelo professor #{professor.nome}"
    end

end