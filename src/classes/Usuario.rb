require 'date'

class Usuario

    attr_accessor :nome, :senha, :email, :nascimento, :logado

    def initialize(email:, senha:, nome:, nascimento:)
        @email = email
        @senha = senha
        @nome = nome
        @nascimento = Date.strptime(nascimento, '%d/%m/%Y')
        @logado = false
    end

    def logar(entrada = '')
        if (entrada == @senha)
            @logado = true
            puts "#{@nome} está logado"
        else
            puts "Não foi possível logar o usuário #{@nome}"
        end
    end

    def deslogar()
        @logado = false
        puts "#{@nome} deslogou"
    end

    def idade()
        return ((Date.today - @nascimento) / 365.to_f).round()
    end

end

